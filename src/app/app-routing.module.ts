import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageMainComponent } from './features/page-main/page-main.component';
import { PageChallengeComponent } from './features/page-challenge/page-challenge.component';

const routes: Routes = [
    { path: '', component: PageMainComponent },
    { path: 'challenge/:letter', component: PageChallengeComponent },
    { path: '**',  redirectTo: '/'},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
