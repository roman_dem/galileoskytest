import { Component, Input, OnInit } from '@angular/core';
import { IAlphabetItem } from '../../core/interfaces';

@Component({
  selector: 'app-widget-letter',
  templateUrl: './widget-letter.component.html',
  styleUrls: ['./widget-letter.component.scss']
})
export class WidgetLetterComponent implements OnInit {
  @Input() currentLetter: IAlphabetItem;
  constructor() { }

  ngOnInit() {
  }

}
