import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { IAlphabetItem } from '../../core/interfaces';

@Component({
    selector: 'app-widget-alphabet',
    templateUrl: './widget-alphabet.component.html',
    styleUrls: ['./widget-alphabet.component.scss']
})
export class WidgetAlphabetComponent implements OnInit, OnChanges {
    @Input() alphabet: IAlphabetItem[];
    @Input() currentLetter: IAlphabetItem | null = null;
    @Output() change = new EventEmitter<IAlphabetItem>();

    constructor() {
    }

    ngOnInit() {
    }

    ngOnChanges(changes) {
        this.currentLetter = changes.currentLetter && changes.currentLetter.currentValue;
    }

    letterClick(letter: IAlphabetItem) {
        if (!this.isDisabled(letter)) {
            this.change.emit(letter);
        }
    }

    isDisabled(letter: IAlphabetItem): boolean {
        return !letter.words.length;
    }

}
