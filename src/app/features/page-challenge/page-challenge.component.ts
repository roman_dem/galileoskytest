import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConstMessages } from '../../core/constants';
import { IAlphabetItem } from '../../core/interfaces';
import { AlphabetService } from '../services/alphabet.service';
import { TaskService } from '../services/task.service';

@Component({
    selector: 'app-page-challenge',
    templateUrl: './page-challenge.component.html',
    styleUrls: ['./page-challenge.component.scss']
})
export class PageChallengeComponent implements OnInit {
    letter: IAlphabetItem;
    alphabet: IAlphabetItem[];
    taskWord: string;

    constructor(private route: ActivatedRoute, private router: Router, private alphabetService: AlphabetService) {
    }

    ngOnInit() {
        this.initData();
        this.route.params.subscribe(async params => {
            const letterFromRoute = params.letter;
            const isValidLetter = this.alphabetService.isValidLetter(letterFromRoute);
            if (isValidLetter) {
                this.letter = this.alphabetService.getLetterData(letterFromRoute);
                this.nextTask();
            } else {
                console.warn(ConstMessages.symbolError);
                await this.router.navigate(['/']);
            }
        });
    }

    public nextTask() {
        this.taskWord = TaskService.getTaskWord(this.letter)
    }

    private initData() {
        this.alphabet = this.alphabetService.getAlphabet()
    }

    private letterChanged(letter: IAlphabetItem) {
        this.router.navigate(['/challenge', letter.letter]).catch();
    }

}
