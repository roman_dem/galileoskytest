import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatButtonModule,
    MatIconModule, MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule
} from '@angular/material';

@NgModule({
    declarations: [],
    imports: [
        MatMenuModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        CommonModule,
        MatSidenavModule
    ],
    exports: [
        MatMenuModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule
    ]
})
export class MaterialModule {
}
