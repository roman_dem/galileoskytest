import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './navigation/header/header.component';
import { LayoutComponent } from './layout/layout.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PageMainComponent } from './page-main/page-main.component';
import { CoreModule } from '../core/core.module';
import { PageChallengeComponent } from './page-challenge/page-challenge.component';
import { AppRoutingModule } from '../app-routing.module';
import { WidgetAlphabetComponent } from './widget-alphabet/widget-alphabet.component';
import { WidgetLetterComponent } from './widget-letter/widget-letter.component';
import { WidgetTaskComponent } from './widget-task/widget-task.component';
import { AlphabetService } from './services/alphabet.service';
import { TaskService } from './services/task.service';

@NgModule({
    declarations: [
        LayoutComponent,
        HeaderComponent,
        PageMainComponent,
        PageChallengeComponent,
        WidgetAlphabetComponent,
        WidgetLetterComponent,
        WidgetTaskComponent
    ],
    imports: [
        CommonModule,
        CoreModule,
        FlexLayoutModule,
        MaterialModule,
        BrowserAnimationsModule,
        AppRoutingModule,
    ],
    exports: [
        MaterialModule,
        LayoutComponent,
        HeaderComponent,
        PageMainComponent,
    ],
    providers: [AlphabetService, TaskService]
})
export class FeaturesModule {
}
