import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { IAlphabetItem } from '../../core/interfaces';
import { CTaskResult } from '../../core/constants';
import { delay, finalize, takeUntil, tap } from 'rxjs/operators';
import { of, Subject } from 'rxjs';

@Component({
    selector: 'app-widget-task',
    templateUrl: './widget-task.component.html',
    styleUrls: ['./widget-task.component.scss']
})
export class WidgetTaskComponent implements OnDestroy {
    @Input() taskWord: string;
    @Input() currentLetter: IAlphabetItem;
    @Output() taskCompleted = new EventEmitter();

    taskResult = null;
    inputDisabled = false;
    private destroyComponent$ = new Subject();

    ngOnChanges() {
        this.reInit();
        this.destroyComponent$ = new Subject();
    }

    ngOnDestroy(): void {
        this.reInit();
    }

    checkUserAnswer(letter: string) {
        this.setTaskResult(letter);
        of(null).pipe(
            tap(() => this.disableInput()),
            delay(2000),
            takeUntil(this.destroyComponent$),
            tap(() => this.taskComplete()),
            finalize(() => this.enableInput())
        ).subscribe();
    }

    private reInit() {
        this.destroyComponent$.next();
        this.destroyComponent$.complete();
        this.taskResult = null;
    }

    private setTaskResult(userLetter) {
        this.taskResult = userLetter === this.currentLetter.letter ? CTaskResult.RIGHT : CTaskResult.WRONG;
    }

    private disableInput() {
        this.inputDisabled = true;
    }

    private enableInput() {
        this.taskResult = null;
        this.inputDisabled = false;
    }

    private taskComplete() {
        if (this.taskResult.success) {
            this.taskCompleted.emit(this.taskResult.success);
        }
        this.enableInput();
    }

}
