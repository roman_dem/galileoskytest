import { Injectable } from '@angular/core';
import { IAlphabetItem } from '../../core/interfaces';
import { LocalStorageService } from '../../core/services/local-storage.service';
import { ConstAlphabetInitData, LocalStorageKeyAlphabet } from '../../core/constants';

@Injectable()
export class AlphabetService {
    private alphabet: IAlphabetItem[];

    constructor() {
        this.initData();
    }

    getAlphabet() {
        return this.alphabet;
    }

    getLetterData(letter): IAlphabetItem {
        return this.alphabet.find(l => {
            return l.letter === letter;
        });
    }

    isValidLetter(letter): boolean {
        return !!this.alphabet.find(v => v.letter === letter && !!v.words.length)
    }

    private initData() {
        let storageData = <IAlphabetItem[]>LocalStorageService.get(LocalStorageKeyAlphabet);
        if (!storageData) {
            LocalStorageService.set(LocalStorageKeyAlphabet, ConstAlphabetInitData);
            storageData = ConstAlphabetInitData;
        }
        this.alphabet = storageData
    }

}
