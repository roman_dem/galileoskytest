import { Injectable } from '@angular/core';
import { IAlphabetItem } from '../../core/interfaces';

@Injectable()
export class TaskService {

    static getTaskWord(letter: IAlphabetItem): string {
        const words = letter.words;
        return words[Math.floor(Math.random() * words.length)];
    }
}
