export interface IAlphabetImg {
    url: string;
    title: string;
}

export interface IAlphabetItem {
    letter: string;
    words: string[];
    img?: IAlphabetImg
}
