import { IAlphabetItem } from './interfaces';

export const LocalStorageKeyAlphabet = 'alphabet';

export const CTaskResult = {
    RIGHT: { text: 'Правильно!', class: 'right', success: true },
    WRONG: { text: 'Неправильно!', class: 'wrong', success: false },
};

export const ConstMessages = {
    symbolError: 'этот символ не определён',
};

export const ConstAlphabetInitData: IAlphabetItem[] = [
    {
        letter: 'а',
        words: ['трактор', 'аэропорт', 'школа', 'вокзал'],
        img: { title: 'арбуз', url: 'watermelon.jpg', }
    },
    {
        letter: 'б',
        words: ['баклажан', 'ребёнок', 'рубашка', 'рыба'],
        img: { title: 'банан', url: 'bananas.jpg', }
    },
    {
        letter: 'в',
        words: ['автобус', 'валенки', 'автограф', 'адвокат'],
        img: { title: 'вишня', url: 'cherry.jpg', }
    },
    {
        letter: 'г',
        words: ['вагон', 'грузовик', 'голубь', 'игра'],
        img: { title: 'груша', url: 'pear.jpg', }
    },
    { letter: 'д', words: [] },
    { letter: 'е', words: [] },
    { letter: 'ё', words: [] },
    { letter: 'ж', words: [] },
    { letter: 'з', words: [] },
    { letter: 'и', words: [] },
    { letter: 'й', words: [] },
    { letter: 'к', words: [] },
    { letter: 'л', words: [] },
    { letter: 'м', words: [] },
    { letter: 'н', words: [] },
    { letter: 'о', words: [] },
    { letter: 'п', words: [] },
    { letter: 'р', words: [] },
    { letter: 'с', words: [] },
    { letter: 'т', words: [] },
];

