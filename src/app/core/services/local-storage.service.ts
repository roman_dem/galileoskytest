import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

    static set<T>(key: string, data: T): void {
        try {
            localStorage.setItem(key, JSON.stringify(data));
        } catch (e) {
            console.log('localStorage setItem err', e.name);
        }
    }

    static get<T>(key: string): T {
        try {
            return JSON.parse(localStorage.getItem(key));
        } catch (e) {
            console.log('localStorage getItem err', e.name);
        }
    }

    static remove(key: string): void {
        try {
            localStorage.removeItem(key);
        } catch (e) {
            console.log('localStorage removeItem err', e);
        }
    }
}
